module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
        less: {
  			application: {
	            options: {
	                paths: ['./src/'],
	                yuicompress: true
	            },
	            files: {
	                './dist/bootstrap-pi.css':  './src/boot.less'
	            }
    		}
		}
	});
    grunt.loadNpmTasks('grunt-contrib-less');
	grunt.registerTask('default', ['less']);
};